# Web Sockets

### Submitted by: Sunil Prajapati

### ID: 124073


A basic example of web socket impelementation using Javascript.

To run the web socker server, please go inside the server directory:

First, install the packages using the command

`yarn install`

To start the server, run the command:

`node index.js`

The server will start at http://localhost:3000

To start the frontend, go to the app directory, and open the `index.html` file in the web browser
